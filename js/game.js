// Fonction jQuery : exécute le jeu une fois que le DOM est chargé

$(function() {

	/* ---------- ---------- */
	/* ----- SETTINGS ------ */
	/* ---------- ---------- */

	// Objet littéral qui stocke l'argent, les vies et la vitesse du jeu
	var	Player = {
			money: 150,
			life : 14,
			speed: 50, // 10 = fast; 50 = normal mode
			time : 10, // time (in sec) before monsters move
			level: 1,
			bestlvl: 1,
			player: 0,
			score: 0,
			bestScore: 0,

		}; 


	// On définit les variables du jeu dans un objet littéral
	var Settings = {
		bestScore1: 0,
	};

	// On attend que l'utilisateur clique sur le bouton "submit" pour lancer le jeu : il valide le formulaire
	$("form").on("submit", function (e) {
    	e.preventDefault(); // On ne recharge pas la page avec le formulaire

    	// On récupère le pseudo rentré par l'utilisateur
    	Player.pseudo = $('input[type=text]#pseudo').val();

    	// On récupère la durée du jeu rentrée par l'utilisateur
    	//Player.speed = $('select[type=number]#speed').val();

    	// On lance le jeu
		initial(Settings);
		//makeMonsters(monsters, Parcours);
		startGame(Player, Parcours, monsters, towers);

	});

	/* ---------- ---------- */
	/* ------ PARCOURS ----- */
	/* ---------- ---------- */

	// Objet littéral qui stocke le parcours des monstres
	var	Parcours = {
			start: 200, 
			sizeCourse: 120,
			course: [
				['down' ,200],
				['right' ,1000],
				['down' ,200],
				['left' ,1000],
				['down' ,210],
				['right' ,1300],
				['up' ,610],

			]
		};

	// On appelle la fonction qui crée le parcours (visuel)
	makeCourse(Parcours);

	/* ---------- ---------- */
	/* ------ TOWERS ------- */
	/* ---------- ---------- */

	var towers = [];  // Tableau qui stocke toutes les tours du jeu

	// On affiche les tours que l'on peut créer à l'écran
	displayTowers(Player, towers); 

	// On appelle la fonction qui permet de créer des tours
	makeTowers(towers, Player);

	/* ---------- ---------- */
	/* ----- MONSTERS ------ */
	/* ---------- ---------- */

	var	monsters = []; // Tableau qui stocke tous les monstres du jeu
	
	// On appelle la fonction qui permet de créer des monstres
	makeMonsters(monsters, Parcours);

	/* ---------- ---------- */
	/* ------- GAME -------- */
	/* ---------- ---------- */
	initial(gameSettings);
	// On appelle la fonction qui lance le jeu
	//startGame(Player, Parcours, monsters, towers);
})

// ------------------------------------------------------------------------- //
// ----------------------- ALL FUNCTIONS FOR THE GAME ---------------------- //
// ------------------------------------------------------------------------- //

// ----------------------
// --- FUNCTIONS GAME ---
// ----------------------

// Fonction initial : initialise les variables, lance le décompte et exécute le jeu
function initial(gameSettings) {

	// On initialise les variables du jeu :
	gameSettings.score = 0;
	gameSettings.timer = gameSettings.timeToPlay; // Permet de réinitialiser le timer à chaque lancement du jeu

	// On affiche le timer dans le HTML
	//$('span.timeLeft').text(gameSettings.timer + ' sec');
	
	// On affiche le score dans le HTML
	//$('h2 span.score').text(gameSettings.score);

	// On affiche le pseudo de l'utilisateur dans le HTML
	//$('h1 span.pseudo').text(gameSettings.pseudo);

	// On masque le div contenant le jeu
	$('.game').hide();
		$('.end').hide();
	// On masque le div contenant les informations du formulaire
	$('div.forms').fadeOut();

	// On affiche le décompte du jeu
	$('div.compteur h2').hide('slow',function(){
		$(this).text('Are');
	}).fadeIn('slow').fadeOut('slow',function(){
		$(this).text('You');
	}).fadeIn('slow').fadeOut('slow',function(){
		$(this).text('Ready ?');
	}).fadeIn('slow').fadeOut('slow',function(){
		$(this).text('Go!');
	}).fadeIn('slow').fadeOut(2000,function() {

		// On affiche le jeu
		$('div.game').fadeIn();


		// On lance le timer

	});
}

// Fonction qui déclare les monstres à créer et les stocke dans le tableau des monstres
function makeMonsters(monsters, Parcours, Player) {
	var MonsterToCreate;

	// On crée l'ensemble des monstres que l'on stocke dans un tableau
	
	for ( var i = 0, max = 3; i < max; i++) {
		// On crée un monstre
		MonsterToCreate = new Monster(-100*(i+1), Parcours.start, (i+1)*100, 'Comix', 3, 'resources/iconfinder_comix.png');
		monsters.push(MonsterToCreate);
	}
}
function makeMonstersV2(monsters, Parcours, Player) {
	var MonsterToCreate;

	// On crée l'ensemble des monstres que l'on stocke dans un tableau
	
	for ( var i = 0, max = 2+(Player.level); i < max; i++) {
		// On crée un monstre
		MonsterToCreate = new Monster(-100*(i+1), Parcours.start, (i+1)*100, 'Comix', getRandomInt(50), 'resources/iconfinder_comix.png');
		monsters.push(MonsterToCreate);
	}
}
function makeMonstersBOSS(monsters, Parcours, Player) {
	var MonsterToCreate;

	// On crée l'ensemble des monstres que l'on stocke dans un tableau
	
	for ( var i = 0, max = (Player.level); i < max; i++) {
		// On crée un monstre
		MonsterToCreate = new Monster(-100*(i+1), Parcours.start, (i+1)*150, 'Slimer', getRandomInt(80), 'resources/iconfinder_Slimer.png');
		monsters.push(MonsterToCreate);
	}
}
// Fonction qui lance le jeu
function startGame(Player, Parcours, monsters, towers) {
	// On affiche les informations du joueur (html)
	$('.infos span.time').text(Player.time);
	$('.infos span.life').text(Player.life);
	$('.infos span.money').text(Player.money);
	$('.infos span.level').text(Player.level);
	$('.infos span.pseudo').text(Player.pseudo);

	// On lance le décompte
	var timer = setInterval(function() {
		$('.infos span.time').text(Player.time); // On change chaque seconde le temps restant
		if (Player.time <= 0) {

			// On arrête le décompte
			clearInterval(timer);

			// On lance le timer pour déplacer les monstres et attaquer
			monsterMove(Player, Parcours, monsters, towers, Player.speed);
		}
		else {
			Player.time--;

		}
	}, 1000);
}	
//Fonction qui permet de définir le message au afficher en fonction du score
function bestScore(Player) {
	$('.pseudo').html(Player.pseudo);
	$('.level').html(Player.level);
	$('.bestLevel').html(Player.bestlvl);
	if (Player.level>Player.bestlvl) {
					Player.bestlvl=Player.level;
					$('#Cas').text('Tu as battu le reccord de '+Player.bestPlayer+' en ateignant le niveau '+Player.bestlvl);
					Player.bestPlayer = Player.pseudo;
				}else {
					$('#Cas').text('Le reccord de '+Player.bestPlayer+' est de '+Player.bestlvl+' essaye de le battre.');
				}
}
$('#btn2').click(function(event) {
	$('#staticBackdrop').modal('hide');	
	$('#item01').css('display', 'block');
	$('#item02').css('display', 'none');
	$('#btn1').css('display', 'block');
	$('#btn2').css('display', 'none');
});

//fonction gerant le bouton Recommencer
$('#Recommencer').click(function() {
	document.location.reload(true);
});

//fonction gerant le bouton Reessayer
($('#Reessayer').click(function() {
$('#item01').css('display', 'none');
$('#item02').css('display', 'block');
$('#btn1').css('display', 'none');
$('#btn2').css('display', 'block');
}));
// Comprend les différents écouteurs d'évènements
/*function GameOver(Player, Parcours, monsters, towers) {

		if (Player.life<=0){
			clearInterval(monsterMove);

			for (var i = 0; i < monsters.length; i++){

				monsters.splice(i,1);
				$('.game .monsters .monster').fadeOut();
				clearInterval(monsterMove)
			}

			for (var i = 0; i < towers.length; i++){

				towers.splice(i);
				$('.game .towers .tower').fadeOut();
				clearInterval(monsterMove)
			}

			afficherGameOver(Player);
			score(Player);
			restart(Player, Parcours, monsters, towers);
			} else{};
		}


function afficherGameOver(Player, Parcours, monsters, towers){
			// On modifie les informations de fin du jeu
			$('.end span.score').text(Player.score);
			$('.end span.playerName').text(Player.pseudo);

			// On affiche les inforamtions
			$('.end').fadeIn();
			$('.tryAgain').click(function() {

			// On masque les informations
			$('.end').fadeOut();

			// On relance le jeu
			initial(gameSettings);
			
			});

			// Si l'utilisateur a battu son propre record :
			if (Player.score > Player.bestScore) {
				Player.bestScore = Player.score; // On enregistre le meilleur score du joueur
				$('.infos-game .bestScore').text('(' + Player.bestScore + ')'); // On affiche le meilleur score du joueur
			}
}*/

// ----------------------
// -- FUNCTIONS OTHERS --
// ----------------------

// Fonction qui calcule l'hypotenuse
function calcHypotenuse(a, b) {
  return(Math.sqrt((a * a) + (b * b)));
}

// Fonction qui retourne une valeur comprise en % d'un chiffre
function hpPourcent (hp, hpMax) {
	return parseInt(hp * 100 / hpMax);
}
// Fonction qui retourne un entier compris entre 0 et l'argument renseigné
function getRandomInt(max=50) {
  return Math.floor(Math.random() * Math.floor(max));
}



